﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {

    Rigidbody rigidBody;
    AudioSource audioSource;

    [SerializeField]
    float rcsThrust = 100f;

    [SerializeField]
    float mainThrust = 1.5f;

    [SerializeField]
    AudioClip mainEngine;

    [SerializeField]
    AudioClip deathSound;

    [SerializeField]
    AudioClip winSound;

    [SerializeField]
    ParticleSystem mainEngineParticle;

    [SerializeField]
    ParticleSystem deathParticle;

    [SerializeField]
    ParticleSystem winParticle;



    enum State {
        Alive, Dying, Transcending
    }

    State state = State.Alive;

    // Start is called before the first frame update
    void Start() {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {
        if ( state == State.Alive) {
            ProcessInput();
        }
    }

    private void ProcessInput() {
        Thurst();
        Rotate();
    }

    private void Thurst() {
        if (Input.GetKey(KeyCode.Space)) {
            ApplyThrust();
        } else {
            audioSource.Stop();
            mainEngineParticle.Stop();
        }
    }

    private void ApplyThrust() {
        rigidBody.AddRelativeForce(Vector3.up * mainThrust);
        if (!audioSource.isPlaying) {
            audioSource.PlayOneShot(mainEngine);
        }
        mainEngineParticle.Play();
    }

    private void Rotate() {

        rigidBody.freezeRotation = true;

        float rotationThisFrame = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A)) {
            transform.Rotate(Vector3.forward * rotationThisFrame);
        } else if (Input.GetKey(KeyCode.D)) {
            transform.Rotate(-Vector3.forward * rotationThisFrame);
        }

        rigidBody.freezeRotation = false;
    }

    private void OnCollisionEnter(Collision collision) {

        if (state != State.Alive) { return; }

        switch (collision.gameObject.tag) {
            case "Friendly":
                break;
            case "Finish":
                StartSuccessSequence();
                break;
            default:
                StartDeathSequence();
                break;
        }
    }

    private void StartDeathSequence() {
        state = State.Dying;
        audioSource.Stop();
        audioSource.PlayOneShot(deathSound);
        deathParticle.Play();
        Invoke("LoadLevelOne", 1f);
    }

    private void StartSuccessSequence() {
        state = State.Transcending;
        audioSource.Stop();
        audioSource.PlayOneShot(winSound);
        winParticle.Play();
        Invoke("LoadNextLevel", 1f);
    }

    private void LoadNextLevel() {
        SceneManager.LoadScene(1);
        audioSource.Stop();
    }

    private void LoadLevelOne() {
        SceneManager.LoadScene(0);
        audioSource.Stop();
    }
}
